// This is a program that reverses a string using three different methods.
//
// Note: endl is used instead of \n, which flushes the output buffer each time.
//      This is not as efficient, but it is used here for demonstration purposes.

#include <iostream>
#include <string>
#include <stack>

enum Method
{
    FOR_LOOP,
    STACK,
    RECURSION,
    END
};

std::string reverseStringForLoop(const std::string& str)
{
    // declare reversedStr to store reversed string
    std::string reversedStr;

    // reserve space for reversedStr
    reversedStr.reserve(str.length());

    // iterate over str in reverse order
    for (int i = str.length() - 1; i >= 0; i--)
    {
        // append current character to reversedStr
        reversedStr += str[i];
    }

    // print reversedStr
    return reversedStr;
}

std::string reverseStringStack(const std::string& str)
{
    // declare reversedStr to store reversed string
    std::string reversedStr;

    // reserve space for reversedStr
    reversedStr.reserve(str.length());

    // declare stack to store characters
    std::stack<char> stack;

    for (char c : str)
    {
        // push current character to stack
        stack.push(c);
    }

    while (!stack.empty())
    {
        // append top character to reversedStr
        reversedStr += stack.top();

        // pop top character from stack
        stack.pop();
    }
    return reversedStr;
}


// Note: this function is not ideal because it creates a new string each time it is called.
std::string reverseStringRecursion(const std::string& str, int index = 0)
{
    if (index == str.length())
    {
        return "";
    }
    return reverseStringRecursion(str, index + 1) + str[index];
}

std::string reverseString(const std::string& str, Method method)
{
    std::string output;

    switch (method)
    {
    case FOR_LOOP:

        output = reverseStringForLoop(str);
        std::cout << "Done." << std::endl;
        break;
    case STACK:
        output = reverseStringStack(str);
        std::cout << "Done." << std::endl;
        break;
    case RECURSION:
        output = reverseStringRecursion(str);
        std::cout << "Done." << std::endl;
        break;
    case END:
        output = "Invalid method!";
        break;
    }
    return output;
}

int getMethodNumber(const std::string& input)
{
    try
    {
        return std::stoi(input);
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what() << std::endl;
    }
    return 0;
}

std::string getMethodString(Method method)
{
    std::string methodString;

    switch (method)
    {
    case FOR_LOOP:
        methodString = "basic for loop";
        break;
    case STACK:
        methodString = "stack";
        break;
    case RECURSION:
        methodString = "recursive";
        break;
    case END:
        methodString = "invalid";
        break;
    }
    return methodString;
}

int main()
{
    // declare str to store user input
    std::string str;

    int methodNumber;

    // declare method to store user input
    Method method;

    // prompt user to enter a string
    std::cout << "Enter a string to reverse: ";

    // read input from user and assign it to str
    std::getline(std::cin, str);

    // prompt user to choose an option
    std::cout << "Enter the number corresponding to the desired reversal method..." << std::endl;
    std::cout << FOR_LOOP + 1 << ". " << getMethodString(FOR_LOOP) << std::endl;
    std::cout << STACK + 1 << ". " << getMethodString(STACK) << std::endl;
    std::cout << RECURSION + 1 << ". " << getMethodString(RECURSION) << std::endl;

    // read input from user and assign it to method
    std::cin >> methodNumber;
    methodNumber -= 1;

    // check if method is valid
    if (methodNumber >= 0 && methodNumber < Method::END)
    {
        method = static_cast<Method>(methodNumber);
        std::cout << "Reversing string using " << getMethodString(method) << " method..." << std::endl;
    }
    else
    {
        method = Method::END;
    }

    // reverse and print string
    std::cout << reverseString(str, method) << std::endl;
}